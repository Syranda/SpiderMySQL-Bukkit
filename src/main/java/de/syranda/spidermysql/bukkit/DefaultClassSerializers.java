package de.syranda.spidermysql.bukkit;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import de.syranda.spidermysql.serializer.ClassSerializer;
import de.syranda.spidermysql.serializer.SerializerInformation;

public class DefaultClassSerializers {

	public static void loadDefaultClassSerializers() {
		
		ClassSerializer.addClassSerializer(Location.class, new ClassSerializer<Location>() {
			
			@Override
			public SerializerInformation<Location> serialize(String fieldName) {

				SerializerInformation<Location> ret = new SerializerInformation<>();
								
				ret.addDouble(fieldName + "_x", loc -> loc.getX());
				ret.addDouble(fieldName + "_y", loc -> loc.getY());
				ret.addDouble(fieldName + "_z", loc -> loc.getZ());
				ret.addDouble(fieldName + "_yaw", loc -> loc.getYaw());
				ret.addDouble(fieldName + "_pitch", loc -> loc.getPitch());
				ret.addString(fieldName + "_world", loc -> loc.getWorld().getName());
				
				return ret;

			}
			
			@Override
			public Location deserialize(String fieldName, HashMap<String, Object> columns) {

				Location loc = new Location(Bukkit.getWorld(columns.get(fieldName + "_world").toString()), Double.parseDouble(columns.get(fieldName + "_x").toString()), Double.parseDouble(columns.get(fieldName + "_y").toString()), Double.parseDouble(columns.get(fieldName + "_z").toString()));
				
				if(columns.containsKey(fieldName + "_pitch"))
					loc.setPitch(Float.parseFloat(columns.get(fieldName + "_pitch").toString()));
				if(columns.containsKey(fieldName + "_yaw"))
					loc.setYaw(Float.parseFloat(columns.get(fieldName + "_yaw").toString()));
								
				return loc;
				
			}
		});
		
	}
	
}
