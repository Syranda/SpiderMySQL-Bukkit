package de.syranda.spidermysql.bukkit.commands;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.adc.Command;
import de.syranda.spidermysql.ConnectionManager;

@Command(value = "spidermysql", aliases = "ssql")
public class SpiderMySQLCommand {

	@Command
	public void ping(CommandSender sender) {		
		try {
			ResultSet rs = ConnectionManager.resultStatement("SELECT 1");
			if (rs.next()) {
				sender.sendMessage(":sConnection is working");
			}
		} catch (SQLException e) {
			sender.sendMessage(":fConnection isn't working");
		}
	}
	
}
